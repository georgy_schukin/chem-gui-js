"use strict";

var ChemGUI = ChemGUI || {};

var jmolApplet = null;

(function (chemgui) {
	function initApplet($appletPlace) {	
		var info = {
			width: "100%",
			height: "100%",
			debug: false,			
			color: "green",
			addSelectionOptions: false,
			serverURL: "http://chemapps.stolaf.edu/jmol/jsmol/php/jsmol.php",
			use: "HTML5 WEBGL",  		  		
			j2sPath: "js/lib/j2s"		
		};

		Jmol.setDocument(false);
		Jmol.getApplet("jmolApplet", info);		
		$appletPlace.html(Jmol.getAppletHtml(jmolApplet));			

		return jmolApplet;
	}

	function loadFile() {
		Jmol.loadFileFromDialog(jmolApplet);
	}

	function runScript(script) {		
		Jmol.script(jmolApplet, script);
	}	

	chemgui.initApplet = initApplet;
	chemgui.loadFile = loadFile;
	chemgui.runScript = runScript;

})(ChemGUI);
