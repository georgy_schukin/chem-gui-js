# chem-gui-js
GUI for ChemGUI with JavaScript and JSMol

# Installation

1. Place j2s folder from Jmol/JSmol distribution into `app/js/lib` folder.
2. Run local HTTP server with `app` being its root directory.
3. Open `http://<server ip>:<server port>/index.html` in a browser.
